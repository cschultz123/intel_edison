# add opkg repositories
echo "src/gz all http://repo.opkg.net/edison/repo/all" >> /etc/opkg/base-feed.conf
echo "src/gz edison http://repo.opkg.net/edison/repo/edison" >> /etc/opkg/base-feed.conf
echo "src/gz core2-32 http://repo.opkg.net/edison/repo/core2-32" >> /etc/opkg/base-feed.conf

# update opkg with new repos
opkg update

# move mraa python binding to home directory
cp /usr/lib/python2.7/site-packages/mraa.py . 
cp /usr/lib/python2.7/site-packages/_mraa.so . 

# install tar
opkg install tar

# install miniconda
chmod -o+x Miniconda2-latest-Linux-x86.sh
./Miniconda2-latest-Linux-x86.sh -b -p $HOME/miniconda

# add miniconda to path
echo "export PATH=\"$HOME/miniconda/bin:\$PATH\"" >> ~/.bashrc

# update conda
conda update conda

# install scipy modules
conda install numpy
conda install matplotlib

# install jupyter notebook
conda install jupyter
