## Edison Setup Instructions

Follow the "Get Started with Intel Edison Board" guide to flash the latest image onto the Intel Edison. This guide can be found here <https://software.intel.com/en-us/iot/library/edison-getting-started>. Once you have completed this guide the Intel Edison should have the latest image loaded onto it.

Connect to the Intel Edison using a serial terminal. An option for Windows and Mac OS X is provided below.

Windows:

1. Launch putty.exe
2. Connect to appropriate COM port at 115200, 8N1
3. Press enter when prompted to login.

Mac OS X:

1. Launch terminal.
2. ls /dev/tty.*
3. screen /dev/tty.* 115200

Once you are logged into the Intel Edison via the serial terminal. Execute the following command to connect to a WiFi network with internet access.

    configure_edison --wifi

The next step of the configuration is to enable SSH. This is accomplished with the following command. This will prompt the user to set a login password.

    configure_edison --password

Now that the Intel Edison is connected to the internet it's time to start installing software. To make it trivial a setup script has been created that will install all of the dependencies. First, clone the git repository with the setup script.

    git clone https://bitbucket.org/cschultz123/intel_edison.git

Execute the following commands to run the setup script.

    chmod -o+x setup.sh
    ./setup.sh

Once the installation has completed all of the module dependencies have been installed. The git repository also includes the jupyter notebook, `edison_prototype.ipynb`, that provides some examples of how to send position commands and plot the trajectory. It also includes the jupyter notebook configuration files in `.jupyter` directory.